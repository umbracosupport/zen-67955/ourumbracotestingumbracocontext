﻿using System;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace TesingUmbracoContextOnV7.App_Code
{
    public class HomeController : RenderMvcController
    {
        // GET: Home
        public ActionResult Index()
        {
            var transactionId = Guid.NewGuid().ToString();
            Session.Add(ThankYouController.TransactionIdKey, transactionId);
            var memberId = Members.GetCurrentMemberId();
            return Redirect("https://fake-payment-gateway.euwest01.umbraco.io/?transactionId=" + transactionId);
        }
    }
}